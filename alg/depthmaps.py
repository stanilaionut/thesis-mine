import time
import numpy as np
from PIL import Image
import matplotlib.cm
from predictor import Predictor

def depth_colormap(img):
    fimg = img.astype("float32")
    timg = fimg - fimg.min()
    timg /= timg.max()
    cmap = matplotlib.cm.get_cmap('viridis')
    mapimg = cmap(timg)
    mapimg = np.uint8(mapimg * 255)
    return mapimg


def to_colormap(predictor, image, result):
    """
    predictor - the predictor
    image - 3-dimensional numpy array with three color channels"""
    arr = np.array(image)
    if arr.shape[2] > 3:
        arr = arr[:,:,0:3]
    start_time = time.clock()
    depth = predictor.predict(arr)
    colormap = depth_colormap(depth)
    end_time = time.clock()
    result["depth"] = Image.fromarray(colormap)
    result["time"] = end_time - start_time


def to_info_map(predictor, image, result):
    """
    predictor - the predictor
    image - 3-dimensional numpy array with three color channels"""
    arr = np.array(image)
    if arr.shape[2] > 3:
        arr = arr[:,:,0:3]
    start_time = time.clock()
    depth = predictor.predict(arr)
    end_time = time.clock()
    result["depth"] = depth
    result["time"] = end_time - start_time