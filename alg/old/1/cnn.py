import numpy as np
import tensorflow as tf
import configparser
import nyu_reader

def default_conv2d(input_layer, filters):
    conv = tf.layers.conv2d(
        inputs=input_layer,
        filters=filters,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu)
    return tf.layers.batch_normalization(inputs=conv, axis=2)

# def default_conv2d_transposed(input_layer, filters):
#     return tf.layers.conv2d_transpose(
#         inputs=input_layer,
#         filters=filters,
#         kernel_size=[3, 3],
#         padding="same",
#         activation=tf.nn.relu)
#     # return tf.layers.batch_normalization(inputs=conv, axis=2)


def cnn_model_fn(features, labels, mode):
    input = default_conv2d(input_layer=features["x"], filters=50)
    conv2 = default_conv2d(input_layer=input, filters=50)
    # conv3 = default_conv2d(input_layer=conv2, filters=50)
    pool3 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)
    conv4 = default_conv2d(input_layer=pool3, filters=80)
    pool4 = tf.layers.max_pooling2d(inputs=conv4, pool_size=[2, 2], strides=2)
    conv5 = default_conv2d(input_layer=pool4, filters=80)
    pool5 = tf.layers.max_pooling2d(inputs=conv5, pool_size=[2, 2], strides=2)
    conv6 = default_conv2d(input_layer=pool5, filters=100)
    pool6 = tf.layers.max_pooling2d(inputs=conv6, pool_size=[2, 2], strides=2)
    conv7 = default_conv2d(input_layer=pool6, filters=120)
    conv8 = default_conv2d(input_layer=conv7, filters=1)
    resize = tf.image.resize_bilinear(conv8, size=(480, 640))
    output = tf.reshape(resize, [-1, 480, 640])

    predictions = {
        # Generate predictions (for PREDICT and EVAL mode)
        "depths": output,
        # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
        # `logging_hook`.
        # "probabilities": tf.nn.relu(features=blabla, name=CNN.tensors_to_log["probabilities"])
    }
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss (for both TRAIN and EVAL modes)
    loss = tf.losses.mean_squared_error(labels=labels,predictions=output)

    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(loss=loss, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(labels=labels, predictions=predictions["depths"])
    }
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


def train(images, depths, estimator):
    # logging_hook = tf.train.LoggingTensorHook(tensors={"probabilities": "rmse_tensor"}, every_n_iter=50)
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": images},
        y=depths,
        batch_size=64,
        num_epochs=10,
        shuffle=True)
    estimator.train(input_fn=train_input_fn, steps=20000) #, hooks=[logging_hook])


def evaluate(images, depths, estimator):
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": images},
        y=depths,
        num_epochs=1,
        shuffle=False)
    return estimator.evaluate(input_fn=eval_input_fn)


def main(argv):
    config = configparser.ConfigParser()
    config.read("config.ini")
    data = nyu_reader.NYUReader(config["default"]["nyu_depth_v2_transposed_resized_32"])
    estimator = tf.estimator.Estimator(model_fn=cnn_model_fn, model_dir=config["default"]["model_directory"])
    images = np.array(data.images[0:100])
    depths = np.array(data.depths[0:100])

    train(images, depths, estimator)


if __name__ == "__main__":
    main(None)
