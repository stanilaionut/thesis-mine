import numpy as np
import h5py
import configparser
from PIL import Image
import matplotlib.pyplot as plt


class NYU_Preprocessor:

    def __init__(self, path, dtype):
        self.f = h5py.File(path)
        self.dtype = dtype

    def transform(self):
        images = np.array(self.f["images"], dtype=self.dtype)
        self._images = np.transpose(images, (0, 3, 2, 1)) / 255.

        depths = np.array(self.f["depths"], dtype=self.dtype)
        self._depths = np.transpose(depths, (0, 2, 1))

    def transform_with_resize(self, new_size):
        images = self.f["images"]
        self._images = np.empty([images.shape[0], new_size[1], new_size[0], 3], dtype=self.dtype)
        for i in range(0, images.shape[0]):
            img = self._image_process(images[i], new_size)
            self._images[i,:,:,:] = img.astype(self.dtype)[:,:,:]

        depths = self.f["depths"]
        self._depths = np.empty([depths.shape[0], depths.shape[2], depths.shape[1]], dtype=self.dtype)
        for i in range(0, depths.shape[0]):
            img = self._depth_process(depths[i], new_size)
            self._depths[i,:,:] = img.astype(self.dtype)[:,:]

    def save(self, path):
        file = h5py.File(path, "w")
        file.create_dataset(u"images", data=self._images, dtype=self.dtype)
        file.create_dataset(u"depths", data=self._depths, dtype=self.dtype)
        file.close()

    def transformation_valid(self):
        return self._images.shape[0] == self._depths.shape[0]

    def _image_process(self, image, new_size):
        transposed = np.transpose(np.array(image))
        # resized = self._resize(transposed, new_size)
        return self._normalize(transposed)

    def _resize(self, image, new_size):
        img = Image.fromarray(image, mode="RGB").resize(new_size, resample=Image.LANCZOS)
        return np.array(img)

    def _normalize(self, image):
        return image / 255.

    def _depth_process(self, depth, new_size):
        transposed = np.transpose(np.array(depth))
        return transposed


def main(argv):
    config = configparser.ConfigParser()
    config.read("config.ini")
    new_size = (int(config["default"]["preprocessed_image_width"]), int(config["default"]["preprocessed_image_height"]))
    t = NYU_Preprocessor(config["default"]["nyu_depth_v2"], config["default"]["dtype"])
    print("Transforming images...")
    t.transform_with_resize(new_size)
    print("Finished transforming images")
    if t.transformation_valid():
        path = config["default"]["nyu_depth_v2_transposed_resized_32"]
        print("Writing dataset to " + path)
        t.save(path)
        print("Finished writing dataset")
    else:
        raise Exception("NYU Transformation exception!")

if __name__ == "__main__":
    main(None)