import numpy as np
import h5py


class NYUReader:
    """Class for readind the NYU depth dataset"""

    def __init__(self, path=None):
        """path - string representing the path to the dataset"""
        f = h5py.File(path, "r")
        self._images = f[u"images"]
        self._depths = f[u"depths"]

    @property
    def images(self):
        """Getter for images in the dataset, shape (1449, 480, 640, 3)"""
        return self._images

    @property
    def depths(self):
        """Getter for depths in the dataset, shape (1449, 480, 640)"""
        return self._depths

    @property
    def length(self):
        if (self.images.shape[0] != self.depths.shape[0]):
            raise ValueError('Not all images are labeled!')
        return self.images.shape[0]

    def image(self, index):
        """Return an nparray of shape
        (config.preprocessed_image_height, config.preprocessed_image_width, 3)"""
        return self.images[index]

    def depth(self, index):
        """Return an nparray of shape (config.preprocessed_depth_height, config.preprocessed_depth_width)"""
        return self.depths[index]
