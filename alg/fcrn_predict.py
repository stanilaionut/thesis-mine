from predictor import Predictor
from fcrn.predict import make_network
from PIL import Image
import numpy as np


class FCRNPredictor(Predictor):
    height = 228
    width = 304

    def __init__(self):
        self.net, self.sess, self.input_node = make_network()

    def predict(self, image):
        """image - numpy array with three dimensions (height, width, 3), where depth=3"""
        img = Image.fromarray(image)
        img = img.resize([FCRNPredictor.width, FCRNPredictor.height], Image.ANTIALIAS)
        img = np.array(img).astype('float32')
        img = np.expand_dims(np.asarray(img), axis = 0)
        pred = self.sess.run(self.net.get_output(), feed_dict={self.input_node: img})
        return pred[0,:,:,0]