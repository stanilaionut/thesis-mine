import configparser
from datastructures import Size
from nyu import NYUPreprocessor


def nyu_to_patches():
    config = configparser.ConfigParser()
    config.read("config.ini")
    patch_size = Size(int(config["patch"]["height"]), int(config["patch"]["width"]))
    with NYUPreprocessor(config["nyu"]["v2"], config["data"]["dtype"]) as t:
        print("Transforming images...")
        t.transform(patch_size)
        print("Finished transforming images")
        if t.transformation_valid(patch_size):
            path = config["nyu"]["v2_transposed_patched_32"]
            print("Writing dataset to " + path)
            t.save(path)
            print("Finished writing dataset")
        else:
            raise Exception("NYU Transformation exception!")


if __name__ == "__main__":
    nyu_to_patches()
