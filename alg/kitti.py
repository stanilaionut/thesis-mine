import h5py
import numpy as np
import os
from PIL import Image



class KittiPreprocessor:

    def __init__(self, images_path, depths_path, seed, dtype):
        self.images_path = images_path
        self.depths_path = depths_path
        self.seed = seed
        self.dtype = dtype
    
    def transform(self, newpath):
        images_lst = os.listdir(self.images_path)
        depths_lst = os.listdir(self.depths_path)
        
        positions = np.arange(len(images_lst))
        np.random.seed(self.seed)
        np.random.shuffle(positions)

        images = np.empty([len(images_lst), 1216, 352, 3], dtype=self.dtype)
        depths = np.empty([len(images_lst), int(1216 / 4), int(352 / 4), 1], dtype=self.dtype)

        for i in positions:
            img = self._process_image(self.images_path + "/" + images_lst[i])
            images[i,:,:,:] = img[:,:,:]

            depth = self._process_depth(self.depths_path + "/" + depths_lst[i])
            depths[i,:,:,:] = depth[:,:,:]
        
        file = h5py.File(newpath, "w")
        file.create_dataset(u"images", data=images, dtype=self.dtype)
        file.create_dataset(u"depths", data=depths, dtype=self.dtype)
        file.close()

    def _process_image(self, path):
        img = np.array(Image.open(path), dtype=self.dtype)
        img = np.transpose(img, (1, 0, 2))
        return self._normalize(img)

    def _process_depth(self, path):
        im = Image.open(path)
        width, height = im.size
        rim = im.resize((int(width / 4), int(height / 4)), Image.LANCZOS)
        eim = np.expand_dims(np.array(rim), axis=2)
        tim = np.transpose(eim, (1, 0, 2))
        return self._normalize(tim)

    def _normalize(self, image):
        return image / 255.
