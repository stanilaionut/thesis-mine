import tensorflow as tf



def make_model(weights=None):
    model = tf.keras.Sequential()

    # 1
    model.add(tf.keras.layers.Convolution2D(filters=64,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu,
                                            data_format="channels_last",
                                            input_shape=(None, None, 3)))
    # model.add(tf.keras.layers.BatchNormalization(axis=2))

    # 2
    model.add(tf.keras.layers.Convolution2D(filters=64,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))
    # model.add(tf.keras.layers.BatchNormalization(axis=2))

    # 3
    model.add(tf.keras.layers.Convolution2D(filters=128,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))
    # model.add(tf.keras.layers.BatchNormalization(axis=2))

    # 4
    # model.add(tf.keras.layers.Convolution2D(filters=128,
    #                                         kernel_size=[3, 3],
    #                                         padding="same",
    #                                         activation=tf.nn.relu))
    # model.add(tf.keras.layers.BatchNormalization(axis=2))

    # 5
    model.add(tf.keras.layers.Convolution2D(filters=256,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))
    # model.add(tf.keras.layers.BatchNormalization(axis=2))

    # 6
    # model.add(tf.keras.layers.Convolution2D(filters=256,
    #                                         kernel_size=[3, 3],
    #                                         padding="same",
    #                                         activation=tf.nn.relu))
    # model.add(tf.keras.layers.BatchNormalization(axis=2))

    # # 7
    model.add(tf.keras.layers.Convolution2D(filters=384,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))
    # model.add(tf.keras.layers.BatchNormalization(axis=2))

    # 8
    model.add(tf.keras.layers.Convolution2D(filters=1,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))
    # model.add(tf.keras.layers.Reshape((patch_size.height, patch_size.width)))

    optimizer = tf.keras.optimizers.Adam()
    model.compile(optimizer=optimizer,
                loss=tf.keras.losses.mean_squared_error,
                metrics=['accuracy'])
    model.summary()


    if (weights != None):
        model.load_weights(weights)
    
    

    # print(tf.global_variables())
    # current_scope = tf.contrib.framework.get_name_scope()
    # with tf.variable_scope(current_scope, reuse=True):
    #     adam_iterations = tf.get_variable("Adam/iterations", (), dtype=tf.int64)
    #     print(adam_iterations)
    #     tf.keras.backend.get_session().run(tf.variables_initializer([adam_iterations]))

    return model


def rmse(y_true, y_pred):
    return tf.keras.backend.sqrt(tf.keras.backend.mean(tf.keras.backend.square(y_pred - y_true), axis=-1))

def make_model_1(weights=None):
    model = tf.keras.Sequential()

    # 1
    model.add(tf.keras.layers.Convolution2D(filters=64,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu,
                                            data_format="channels_last",
                                            input_shape=(None, None, 3)))

    # 2
    model.add(tf.keras.layers.AveragePooling2D(data_format="channels_last"))

    # 3
    model.add(tf.keras.layers.Convolution2D(filters=128,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    # 4
    model.add(tf.keras.layers.Convolution2D(filters=256,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    # 5
    model.add(tf.keras.layers.AveragePooling2D(data_format="channels_last"))

    # 6
    model.add(tf.keras.layers.Convolution2D(filters=256,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    # 7
    model.add(tf.keras.layers.Convolution2D(filters=256,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    # 8
    model.add(tf.keras.layers.Convolution2D(filters=1,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    # 9
    model.add(tf.keras.layers.UpSampling2D(size=(4, 4), data_format="channels_last"))

    optimizer = tf.keras.optimizers.Adam()
    model.compile(optimizer=optimizer,
                loss=tf.keras.losses.mean_squared_error,
                metrics=[rmse])
    model.summary()

    if (weights != None):
        model.load_weights(weights)

    return model



def make_model_2(weights=None):
    model = tf.keras.Sequential()

    # 1
    model.add(tf.keras.layers.Convolution2D(filters=64,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu,
                                            data_format="channels_last",
                                            input_shape=(None, None, 3)))

    # 2
    model.add(tf.keras.layers.AveragePooling2D(data_format="channels_last"))

    # 3
    model.add(tf.keras.layers.Convolution2D(filters=256,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    # 4
    model.add(tf.keras.layers.Convolution2D(filters=256,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    # 5
    model.add(tf.keras.layers.AveragePooling2D(data_format="channels_last"))

    # 6
    model.add(tf.keras.layers.Convolution2D(filters=256,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    # 7
    model.add(tf.keras.layers.Convolution2D(filters=256,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    # 8
    model.add(tf.keras.layers.Convolution2D(filters=1,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    # 9
    # model.add(tf.keras.layers.UpSampling2D(size=(4, 4), data_format="channels_last"))

    optimizer = tf.keras.optimizers.Adam()
    model.compile(optimizer=optimizer,
                loss=tf.keras.losses.mean_squared_error,
                metrics=[rmse])
    model.summary()

    if (weights != None):
        model.load_weights(weights)

    return model



def make_model_3(weights=None):
    model = tf.keras.Sequential()

    model.add(tf.keras.layers.Convolution2D(filters=50,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu,
                                            data_format="channels_last",
                                            input_shape=(None, None, 3)))

    model.add(tf.keras.layers.Convolution2D(filters=50,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    model.add(tf.keras.layers.Convolution2D(filters=50,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    model.add(tf.keras.layers.MaxPooling2D(data_format="channels_last"))

    model.add(tf.keras.layers.Convolution2D(filters=80,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    model.add(tf.keras.layers.MaxPooling2D(data_format="channels_last"))

    model.add(tf.keras.layers.Convolution2D(filters=80,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    model.add(tf.keras.layers.MaxPooling2D(data_format="channels_last"))

    model.add(tf.keras.layers.Convolution2D(filters=100,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    model.add(tf.keras.layers.Conv2DTranspose(filters=120,
                                              strides=(2, 2),
                                              kernel_size=[3, 3],
                                              padding="same",
                                              activation=tf.nn.relu))

    model.add(tf.keras.layers.Convolution2D(filters=1,
                                            kernel_size=[3, 3],
                                            padding="same",
                                            activation=tf.nn.relu))

    optimizer = tf.keras.optimizers.Adam()
    model.compile(optimizer=optimizer,
                loss=tf.keras.losses.mean_squared_error,
                metrics=[rmse])
    model.summary()

    if (weights != None):
        model.load_weights(weights)

    return model