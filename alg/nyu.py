import numpy as np
import h5py
from datastructures import Size
from PIL import Image


class NYUPreprocessor:
    """Class for preprocessing the NYU depth dataset."""

    def __init__(self, path, dtype):
        self.path = path
        self.dtype = dtype

    def __enter__(self):
        self.f = h5py.File(self.path, "r")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.f.close()

    def transform(self, patch_size):
        images = self.f["images"]
        depths = self.f["depths"]
        vsplit = int(images.shape[3] / patch_size.height)
        hsplit = int(images.shape[2] / patch_size.width)
        total_size = images.shape[0] * vsplit * hsplit
        batch_size = vsplit * hsplit

        self._images = np.empty([total_size, patch_size.height, patch_size.width, 3], dtype=self.dtype)
        for i in range(0, images.shape[0]):
            img = self._process_image(images[i])
            patches = NYUPreprocessor.segment_image(img, patch_size, self.dtype)
            self._images[i * batch_size:i * batch_size + batch_size,:,:,:] = patches[:,:,:,:]

        self._depths = np.empty([total_size, patch_size.height, patch_size.width, 1], dtype=self.dtype)
        for i in range(0, depths.shape[0]):
            img = self._process_depth(depths[i])
            patches = NYUPreprocessor.segment_image(img, patch_size, self.dtype, channels=1)
            self._depths[i * batch_size:i * batch_size + batch_size,:,:,:] = patches[:,:,:,:]

    def save(self, path):
        file = h5py.File(path, "w")
        file.create_dataset(u"images", data=self._images, dtype=self.dtype)
        file.create_dataset(u"depths", data=self._depths, dtype=self.dtype)
        file.close()

    def transformation_valid(self, patch_size):
        if self._images.shape[0] != self._depths.shape[0]:
            return False
        for i in range(0, self._images.shape[0]):
            if self._images[i].shape[0] != self._depths[i].shape[0] or \
               self._images[i].shape[1] != self._depths[i].shape[1] or \
               self._images[i].shape[0] != patch_size.height or \
               self._images[i].shape[1] != patch_size.width or \
               self._images[i].shape[2] != 3 or \
               self._depths[i].shape[2] != 1:
                return False
        return True

    def _normalize(self, image):
        return image / 255.

    @staticmethod
    def segment_image(img, patch_size, dtype, channels=3):
        vsplit = int(img.shape[0] / patch_size.height)
        hsplit = int(img.shape[1] / patch_size.width)
        vpad = int((img.shape[0] % patch_size.height) / 2)
        hpad = int((img.shape[1] % patch_size.width) / 2)
        patches = np.empty([vsplit * hsplit, patch_size.height, patch_size.width, channels], dtype=dtype)
        ph = patch_size.height
        pw = patch_size.width
        for v in range(0, vsplit):
            for h in range(0, hsplit):
                patches[v * vsplit + h,:,:,:] = img[v * ph + vpad:(v + 1) * ph + vpad,h * pw + hpad:(h + 1) * pw + hpad,:]
        return patches

    # @staticmethod
    # def segment_depth(img, patch_size, dtype):
    #     vsplit = int(img.shape[0] / patch_size.height)
    #     hsplit = int(img.shape[1] / patch_size.width)
    #     vpad = int((img.shape[0] % patch_size.height) / 2)
    #     hpad = int((img.shape[1] % patch_size.width) / 2)
    #     patches = np.empty([vsplit * hsplit, patch_size.height, patch_size.width], dtype=dtype)
    #     ph = patch_size.height
    #     pw = patch_size.width
    #     for v in range(0, vsplit):
    #         for h in range(0, hsplit):
    #             patches[v * vsplit + h,:,:] = img[v * ph + vpad:(v + 1) * ph + vpad,h * pw + hpad:(h + 1) * pw + hpad]
    #     return patches

    def _process_image(self, image):
        transposed = np.transpose(np.array(image))
        return self._normalize(transposed)

    def _process_depth(self, depth):
        transposed = np.transpose(np.array(depth))
        transposed = np.expand_dims(transposed, axis=2)
        return transposed



class NYUImages:
    """Class for nyu images"""

    def __init__(self, path, dtype):
        self.path = path
        self.dtype = dtype
        self.pad = 16

    def __enter__(self):
        self.f = h5py.File(self.path, "r")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.f.close()

    def transform(self, path):
        fimages = self.f["images"]
        fdepths = self.f["depths"]

        images = np.empty([fimages.shape[0], 480 - 2 * self.pad, 640 - 2 * self.pad, 3], dtype=self.dtype)
        for i in range(0, images.shape[0]):
            img = self._process_image(fimages[i])
            images[i,:,:,:] = img[:,:,:]

        # depths = np.empty([
        #     fimages.shape[0],
        #     int((480 - 2 * self.pad) / 4),
        #     int((640 - 2 * self.pad) / 4), 1],
        #     dtype=self.dtype)
        depths = np.empty([fimages.shape[0], 480 - 2 * self.pad, 640 - 2 * self.pad, 1], dtype=self.dtype)
        for i in range(0, depths.shape[0]):
            # img = self._process_depth(fdepths[i])
            img = self._process_depth_no_resize(fdepths[i])
            depths[i,:,:,:] = img[:,:,:]

        file = h5py.File(path, "w")
        file.create_dataset(u"images", data=images, dtype=self.dtype)
        file.create_dataset(u"depths", data=depths, dtype=self.dtype)
        file.close()
    
    def _process_image(self, image):
        transposed = np.transpose(np.array(image))
        normalized = self._normalize(transposed)
        return self._crop(normalized)

    def _process_depth(self, depth):
        transposed = np.transpose(np.array(depth))
        arr = self._crop(transposed)
        im = Image.fromarray(arr)
        width, height = im.size
        rim = im.resize((int(width / 4), int(height / 4)), Image.LANCZOS)
        return np.expand_dims(np.array(rim), axis=2)

    def _process_depth_no_resize(self, depth):
        transposed = np.transpose(np.array(depth))
        arr = self._crop(transposed)
        # im = Image.fromarray(arr)
        # width, height = im.size
        # rim = im.resize((int(width / 4), int(height / 4)), Image.LANCZOS)
        # return np.expand_dims(np.array(rim), axis=2)
        return np.expand_dims(arr, axis=2)

    def _normalize(self, image):
        return image / 255.

    def _crop(self, img):
        height = img.shape[0]
        width = img.shape[1]
        if len(img.shape) == 2:
            return img[self.pad:height - self.pad,self.pad:width - self.pad]
        else:
            return img[self.pad:height - self.pad,self.pad:width - self.pad,:]
