import argparse
import os
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from PIL import Image

import models
        

def make_network():
    # Default input size
    height = 228
    width = 304
    channels = 3
    batch_size = 1
   
    # Create a placeholder for the input image
    input_node = tf.placeholder(tf.float32, shape=(None, height, width, channels))

    # Construct the network
    net = models.ResNet50UpProj({'data': input_node}, batch_size, 1, False)

    sess = tf.Session()

    # Use to load from ckpt file
    saver = tf.train.Saver()     
    saver.restore(sess, "NYU_FCRN.ckpt")
    
    return (net, sess, input_node)


        



