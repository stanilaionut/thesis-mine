import h5py
import sys
import numpy as np
from PIL import Image
from depthmaps import depth_colormap


if __name__ == "__main__":
    f = h5py.File("data/nyu_depth_v2_labeled.mat", "r")
    # np.savetxt(sys.argv[1] + "_ground_truth.txt", f["depths"][int(sys.argv[1])])
    arr = f["depths"][int(sys.argv[1])]

    cmap = depth_colormap(np.transpose(arr))
    cimg = Image.fromarray(cmap)
    cimg.show()

    img = Image.fromarray(arr)
    width, height = img.size
    simg = img.resize((int(width / 4), int(height / 4)), Image.LANCZOS)
    scmap = depth_colormap(np.transpose(np.array(simg)))
    scimg = Image.fromarray(scmap)
    scimg.show()
