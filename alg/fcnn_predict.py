import os
import sys
import tensorflow as tf
import numpy as np
from PIL import Image
import configparser
from predictor import Predictor
import nyu
from datastructures import Size
from depthmaps import depth_colormap
from model import rmse



class FCNNPredictor(Predictor):
    height = 228
    width = 304

    def __init__(self, model_path):
        self.model = tf.keras.models.load_model(filepath=model_path, compile=False)
        self.model.compile(
            optimizer=tf.keras.optimizers.Adam(),
            loss=tf.keras.losses.mean_squared_error,
            metrics=[rmse])
        self.model._make_predict_function()
        self.graph = tf.get_default_graph()

        config = configparser.ConfigParser()
        config.read("config.ini")
        self.patch_size = Size(int(config["patch"]["height"]), int(config["patch"]["width"]))

    # def predict(self, image):
    #     """image - numpy array with three dimensions (height, width, 3), where depth=3"""
    #     patches = self._split(image)
    #     prediction = self.model.predict(x=patches, batch_size=1)
    #     return self._aggregate(image, prediction)

    def predict(self, image):
        """image - numpy array with three dimensions (height, width, 3), where depth=3"""
        res = Image.fromarray(image)
        res.resize([FCNNPredictor.width, FCNNPredictor.height], Image.LANCZOS)
        nimg = np.array(res, dtype="float32") / 255.
        data = np.array([nimg])
        prediction = self.model.predict(x=data)
        return np.reshape(prediction, [prediction.shape[1], prediction.shape[2]])

    def _split(self, image):
        patches = nyu.NYUPreprocessor.segment_image(image, self.patch_size, "uint8")
        return patches

    def _aggregate(self, image, patches):
        vsplit = int(image.shape[0] / self.patch_size.height)
        hsplit = int(image.shape[1] / self.patch_size.width)
        depth = np.empty([vsplit * self.patch_size.height, hsplit * self.patch_size.width])
        pw = self.patch_size.width
        ph = self.patch_size.height
        for v in range(0, vsplit):
            for h in range(0, hsplit):
                depth[v * pw:(v + 1) * pw,h * ph:(h + 1) * ph] = patches[v * vsplit + h,:,:]
        return depth


if __name__ == "__main__":
    if len(sys.argv) == 1 or len(sys.argv) == 2:
        raise Exception("Two arguments required! (image number, checkpoint)")
    else:
        index = sys.argv[1]
        weights = sys.argv[2]

    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    
    predictor = FCNNPredictor("./tmp/weights." + weights + ".hdf5")
    image = np.array(Image.open("./predictions/" + index + "_test_image.bmp"))
    depth = predictor.predict(image)
    np.savetxt("./predictions/" + index + "_prediction_" + weights + ".txt", depth)
    colormap = depth_colormap(depth)
    pd = Image.fromarray(colormap)
    pd.save("./predictions/" + index + "_prediction_" + weights + ".bmp")
    pd.show() # to delete

