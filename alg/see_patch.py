import h5py
import sys
import numpy as np
from PIL import Image
from depthmaps import depth_colormap


if __name__ == "__main__":
    filename = "data/nyu_train.h5"
    f = h5py.File(filename, "r")

    img = Image.fromarray(np.array(f["images"][int(sys.argv[1])] * 255., dtype="uint8"))
    img.show()

    depth = np.reshape(f["depths"][int(sys.argv[1])], [96, 96])
    # np.savetxt("patch.txt", depth)
    cmap = depth_colormap(depth)
    Image.fromarray(cmap).show()
