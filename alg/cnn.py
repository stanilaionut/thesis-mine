import sys
import numpy as np
import tensorflow as tf
from configparser import ConfigParser
from imagetools import ImageReader
from model import make_model_1, make_model_2, make_model_3, rmse
from datastructures import Size




def run(model,
        train_data,
        validation_data,
        tensorboard_directory,
        initial_epoch):

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=.7)
    tf.keras.backend.set_session(tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)))

    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir='./tmp/tensorboard',
                                                          histogram_freq=0,
                                                          write_graph=True,
                                                          write_grads=True,
                                                          write_images=True)
    checkpointer = tf.keras.callbacks.ModelCheckpoint(filepath='./tmp/weights.{epoch:02d}-{loss:.3f}.hdf5',
                                                      monitor='loss',
                                                      verbose=1,
                                                      save_best_only=False)
    tf.keras.backend.get_session().run(tf.global_variables_initializer())    
    # history = model.fit(train_data.images,
    #                     train_data.depths,
    #                     epochs=999,
    #                     initial_epoch=initial_epoch,
    #                     batch_size=1,
    #                     shuffle='batch',
    #                     verbose=1,
    #                     callbacks=[tensorboard_callback, checkpointer])
    evaluation = model.evaluate(validation_data.images,
                                validation_data.depths,
                                batch_size=1,
                                verbose=1)
    # print('History: ', history)
    print('Evaluation: ', evaluation)



if __name__ == '__main__':
    config = ConfigParser()
    config.read("config.ini")
    patch_size = Size(int(config["patch"]["height"]), int(config["patch"]["width"]))
    with ImageReader(config["kitti"]["train"]) as train_data, \
        ImageReader(config["kitti"]["validation"]) as validation_data:
        initial_epoch = 0
        if len(sys.argv) == 1:
            model = make_model_3()
        else:
            # model = make_model_2(weights=sys.argv[1])
            # model = tf.keras.models.load_model(sys.argv[1])
            model = tf.keras.models.load_model(filepath=sys.argv[1], compile=False)
            model.compile(
                optimizer=tf.keras.optimizers.Adam(),
                loss=tf.keras.losses.mean_squared_error,
                metrics=[rmse])
            # model.load_weights(sys.argv[1])
            initial_epoch = int(sys.argv[1].split(".")[1].split("-")[0])
        run(model, train_data, validation_data, config["tensorflow"]["tensorboard_directory"], initial_epoch)
