import h5py
import numpy as np


class ImageReader:
    """Class for reading the NYU depth dataset."""

    def __init__(self, path):
        """path - string representing the path to the dataset"""
        self.path = path

    def __enter__(self):
        self.f = h5py.File(self.path, "r")
        self._images = self.f[u"images"]
        self._depths = self.f[u"depths"]
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.f.close()

    @property
    def images(self):
        return self._images

    @property
    def depths(self):
        return self._depths

    @property
    def length(self):
        if (self.images.shape[0] != self.depths.shape[0]):
            raise ValueError('Not all images are labeled!')
        return self.images.shape[0]

    def image(self, index):
        """Return an nparray of shape
        (config.patch.height, config.patch.width, 3)"""
        return self.images[index]

    def depth(self, index):
        """Return an nparray of shape (config.patch.height, config.patched.width, 1)"""
        return self.depths[index]



class DatasetRandomizer:
    """Class for creating train, evaluation and test sets for various depth datasets."""

    def __init__(self, path, dtype, seed):
        self.path = path
        self.dtype = dtype
        self.seed = seed

    def __enter__(self):
        self.f = h5py.File(self.path, "r")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.f.close()

    def randomize(self, train_ratio, validation_ratio, train_path, validation_path, test_path):
        data_size = self.f["images"].shape[0]
        train_size = int(data_size * train_ratio)
        validation_size = int(data_size * validation_ratio)
        positions = np.arange(data_size)
        np.random.seed(self.seed)
        np.random.shuffle(positions)

        train_file = h5py.File(train_path, "w")
        self._save(train_file, 0, train_size, positions)
        train_file.close()

        validation_file = h5py.File(validation_path, "w")
        self._save(validation_file, train_size, train_size + validation_size, positions)
        validation_file.close()

        test_file = h5py.File(test_path, "w")
        self._save(test_file, train_size + validation_size, data_size, positions)
        test_file.close()

    def _save(self, file, start, end, positions):
        images = self.f["images"]
        subset = np.empty([end - start, images.shape[1], images.shape[2], images.shape[3]], dtype=self.dtype)
        for i in range(start, end):
            subset[i - start,:,:,:] = images[positions[i],:,:,:]
        file.create_dataset(u"images", data=subset, dtype=self.dtype)
        del subset

        depths = self.f["depths"]
        subset = np.empty([end - start, depths.shape[1], depths.shape[2], depths.shape[3]], dtype=self.dtype)
        for i in range(start, end):
            subset[i - start,:,:,:] = depths[positions[i],:,:,:]
        file.create_dataset(u"depths", data=subset, dtype=self.dtype)
        del subset
