class Predictor:
    def predict(self, image):
        """Calculate the depth from an image.
        
        Args:
            image: a 3D numpy array representing the image

        Returns:
            depth_map: a 2D numpy array
        """
        raise NotImplementedError
