import configparser
from datastructures import Size
from nyu import NYUImages


def nyu_process():
    config = configparser.ConfigParser()
    config.read("config.ini")
    with NYUImages(config["nyu"]["v2"], config["data"]["dtype"]) as t:
        print("Transforming images...")
        t.transform(config["nyu"]["v2_processed"])
        print("Finished transforming images")


if __name__ == "__main__":
    nyu_process()
