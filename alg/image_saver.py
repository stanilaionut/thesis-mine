import sys
import numpy as np
from imagetools import ImageReader
import configparser
from PIL import Image
import matplotlib.pyplot as plt
from depthmaps import depth_colormap


def process_depth(depth):
    im_arr = np.array(depth)
    im_arr = np.transpose(im_arr, (1, 0, 2))
    im_arr = np.squeeze(im_arr, axis=2)
    # img_ = np.empty([480, 640])
    # img_[:,:] = im_arr[:,:].T
    return Image.fromarray(depth_colormap(im_arr))


def process_image(image):
    im_arr = np.array(image)
    img = np.transpose(im_arr, (1, 0, 2))
    # img_ = np.empty()
    # img_[:,:,0] = im_arr[0,:,:].T
    # img_[:,:,1] = im_arr[1,:,:].T
    # img_[:,:,2] = im_arr[2,:,:].T
    plt.imshow(img.astype('float32'))
    plt.show()
    return Image.fromarray((img * 255.).astype('uint8'))


def save(index):
    config = configparser.ConfigParser()
    config.read("config.ini")
    with ImageReader(config["kitti"]["test"]) as dataset:
        image = process_image(dataset.image(int(index)))
        image.save("./predictions/" + index + "_k_test_image.bmp")
        depth = process_depth(dataset.depth(int(index)))
        depth.save("./predictions/" + index + "_k_test_depth.bmp")


if __name__ == "__main__":
    if sys == 1:
        save("777")
    else:
        save(sys.argv[1])