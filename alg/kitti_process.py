import configparser
from kitti import KittiPreprocessor


def kitti_process():
    config = configparser.ConfigParser()
    config.read("config.ini")
    print("Transforming images...")
    k = KittiPreprocessor(
        config["kitti"]["image_path"],
        config["kitti"]["depth_path"],
        int(config["data"]["random_seed"]),
        config["data"]["dtype"])
    k.transform(config["kitti"]["path"])
    print("Finished transforming images")


if __name__ == "__main__":
    kitti_process()
