import configparser
from datastructures import Size
from imagetools import DatasetRandomizer


def nyu_randomizer():
    config = configparser.ConfigParser()
    config.read("config.ini")
    dataset = config["kitti"]["path"]
    dtype = config["data"]["dtype"]
    seed = int(config["data"]["random_seed"])
    with DatasetRandomizer(dataset, dtype, seed) as t:
        train_ratio = float(config["nyu"]["train_ratio"])
        validation_ratio = float(config["nyu"]["validation_ratio"])
        train_path = config["kitti"]["train"]
        validation_path = config["kitti"]["validation"]
        test_path = config["kitti"]["test"]
        t.randomize(train_ratio, validation_ratio, train_path, validation_path, test_path)


if __name__ == "__main__":
    nyu_randomizer()
