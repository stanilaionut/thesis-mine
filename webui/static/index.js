document.onreadystatechange = function () {

    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});

    var filename = "depth.jpg";
    
    document.getElementById("img_picker").onchange = function (event) {
        var target = event.target || window.event.srcElement;
        var files = target.files;
    
        if (FileReader && files && files.length) {
            var fileReader = new FileReader();
            fileReader.onload = function () {
                document.getElementById("src_img").src = fileReader.result;
                document.getElementById("compute_btn").removeAttribute("disabled");
            }
            fileReader.readAsDataURL(files[0]);
        }
    }

    document.getElementById("compute_btn").onclick = function (elem, event) {
        var http = new XMLHttpRequest();
        var url = "http://127.0.0.1:8000/depth";
        http.open("POST", url, true);
        http.setRequestHeader("Content-Type", "application/json");
        var algSelection = document.getElementById("alg-selection");
        http.send(JSON.stringify({
            alg: algSelection.options[algSelection.selectedIndex].value,
            image: document.getElementById("src_img").src.split(",")[1]
        }));
        http.onreadystatechange = function () {
            if(http.readyState == 4 && http.status == 200) {
                var dstImg = document.getElementById("dst_img");
                dstImg.src = JSON.parse(http.responseText)["image"];
                var donwloadButton = document.getElementById("download_btn");
                donwloadButton.href = dstImg.src;
                donwloadButton.style.color = "#2BBBAD";
            }
            document.getElementById("working-overlay").style.display = "none";
        }
        document.getElementById("working-overlay").style.display = "flex";
    }

    downloadDepth = function (element) {
        element.href = document.getElementById("dst_img").src;
    }
}