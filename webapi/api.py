import falcon
import mimetypes
import os
import io
from PIL import Image
import base64
import json
from falcon_cors import CORS
import numpy as np

from backend import predict_depth, predict_depth_info, algs


class Depth:
    def on_post(self, req, resp):
        alg = req.media["alg"]
        if int(alg) in algs:
            base64_image = req.media["image"]
            try:
                image = Image.open(io.BytesIO(base64.b64decode(base64_image)))
                depth = predict_depth(alg, image)["depth"]
                output = io.BytesIO()
                depth.save(output, format="PNG")
                im_data = output.getvalue()
                depth_map = "data:image/png;base64," + base64.b64encode(im_data).decode("utf-8")
                resp.status = falcon.HTTP_200
                resp.body = json.dumps({"image": depth_map})
            except:
                resp.status = falcon.HTTP_400
                resp.body = json.dumps({"error": "invalid image data"})
        else:
            resp.status = falcon.HTTP_400
            resp.body = json.dumps({"error": "invalid algorithm"})


class DepthInfo:
    def on_post(self, req, resp):
        alg = req.media["alg"]
        if int(alg) in algs:
            base64_image = req.media["image"]
            try:
                image = Image.open(io.BytesIO(base64.b64decode(base64_image)))
                result = predict_depth_info(alg, image)
                depth = result["depth"]
                resp.body = json.dumps({"image": depth.tolist(), "time": result["time"]})
            except:
                resp.status = falcon.HTTP_400
                resp.body = json.dumps({"error": "invalid image data"})
        else:
            resp.status = falcon.HTTP_400
            resp.body = json.dumps({"error": "invalid algorithm"})


# allow_origins_list=['http://localhost:5000']
cors = CORS(allow_all_origins=True,
            allow_all_headers=True,
            allow_all_methods=True)

api = application = falcon.API(middleware=[cors.middleware])

simpleDepth = Depth()
complexDepth = DepthInfo()

api.add_route("/depth", simpleDepth)
api.add_route("/depthinfo", complexDepth)
