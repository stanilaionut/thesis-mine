import urllib.request
import numpy as np
import base64
from PIL import Image
import io
import json
import matplotlib.cm


def depth_colormap(img):
    fimg = img.astype("float32")
    timg = fimg - fimg.min()
    timg /= timg.max()
    cmap = matplotlib.cm.get_cmap('viridis')
    mapimg = cmap(timg)
    mapimg = np.uint8(mapimg * 255)
    return mapimg


img = Image.open("./1_test_image.bmp")
output = io.BytesIO()
img.save(output, format="PNG")
im_data = output.getvalue()
datadict = {
    "alg": "0",
    "image": base64.b64encode(im_data).decode("utf-8")
}
bdata = json.dumps(datadict).encode("UTF-8")

req = urllib.request.Request(
    url="http://127.0.0.1:8000/depthinfo",
    method="POST",
    data=bdata)
req.add_header("Content-Type", "application/json")
req.add_header("Content-Length", len(bdata))

with urllib.request.urlopen(req) as resp:
    content = json.loads(resp.read().decode("utf-8"))
    print(content["time"])
    dmap = Image.fromarray(depth_colormap(np.array(content["image"])))
    dmap.show()