import os
import sys
from threading import Thread
import tensorflow as tf


parent_path = os.path.abspath("..")
alg_path = os.path.abspath("../alg")
fcrn_path = os.path.abspath("../alg/fcrn")
if parent_path not in sys.path:
    sys.path.append(parent_path)
if alg_path not in sys.path:
    sys.path.append(alg_path)
if fcrn_path not in sys.path:
    sys.path.append(fcrn_path)

from alg.fcrn_predict import FCRNPredictor
from alg.fcnn_predict import FCNNPredictor
from alg.depthmaps import to_colormap, to_info_map

# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

algs = [0, 1, 2, 3, 4]
predictors = {
    "0": FCRNPredictor(),
    "1": FCNNPredictor("./weights.200-0.752.hdf5"),
    "2": FCNNPredictor("./weights.200-0.735.hdf5"),
    "3": FCNNPredictor("./weights.202-0.810.hdf5"),
    "4": FCNNPredictor("./weights.403-5.560.hdf5")
}


def predict_depth(alg, image):
    result = {"depth": None, "time": 0}
    thread = Thread(target=threaded_predict, args=(predictors[alg], image, result))
    thread.start()
    thread.join()
    return result

def threaded_predict(predictor_instance, image, result):
    if not hasattr(predictor_instance, "graph"):
        with predictor_instance.sess.as_default():
            to_colormap(predictor_instance, image, result)
    else:
        with predictor_instance.graph.as_default():
            to_colormap(predictor_instance, image, result)

def predict_depth_info(alg, image):
    result = {"depth": None, "time": 0}
    thread = Thread(target=threaded_predict_info, args=(predictors[alg], image, result))
    thread.start()
    thread.join()
    return result

def threaded_predict_info(predictor_instance, image, result):
    if not hasattr(predictor_instance, "graph"):
        with predictor_instance.sess.as_default():
            to_info_map(predictor_instance, image, result)
    else:
        with predictor_instance.graph.as_default():
            to_info_map(predictor_instance, image, result)